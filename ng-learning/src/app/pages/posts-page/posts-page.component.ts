import { Component, OnInit } from '@angular/core';
import { NgForOf, NgIf } from '@angular/common';
import { switchMap } from 'rxjs';
import { Post, User, UserId } from '../../api/interfaces';
import { PostsClientService } from '../../api/posts-client.service';
import { UsersClientService } from '../../api/users-client.service';

@Component({
  selector: 'posts-page',
  standalone: true,
  imports: [NgIf, NgForOf],
  templateUrl: './posts-page.component.html',
  styleUrl: './posts-page.component.less'
})
export class PostsPageComponent implements OnInit{

  data: DisplayData[] = [];

  constructor(private postsService: PostsClientService, private usersService: UsersClientService) { }

  ngOnInit(): void {
    let apiPosts: Post[] = [];
    let apiUsers: Map<UserId, User> = new Map();
    this.postsService.getAll()
      .pipe(switchMap(posts => {
        apiPosts = posts
        return this.usersService.getByIds(posts.map(p => p.userId))
      }))
      .subscribe((users) => {
        for (const user of users) {
          apiUsers.set(user.id, user);
        }
        this.data = apiPosts.map(post => ({
          post: post,
          user: apiUsers.get(post.userId)!,
        }));
      })
  }
}

interface DisplayData {
  post: Post;
  user: User;
}