import { NgForOf } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [NgForOf, RouterLink, RouterLinkActive],
  templateUrl: './header.component.html',
  styleUrl: './header.component.less'
})
export class HeaderComponent {
  options: NavLinks[] = [
    {
      path: "/",
      displayText: "Home",
    },
    {
      path: "/posts",
      displayText: "Posts",
    },
  ];
}
interface NavLinks {
  path: string;
  displayText: string;
}