// newtype format taken from https://kubyshkin.name/posts/newtype-in-typescript/
export type PostId = number & { readonly __tag: unique symbol };
export type CommentId = number & { readonly __tag: unique symbol };
export type AlbumId = number & { readonly __tag: unique symbol };
export type PhotoId = number & { readonly __tag: unique symbol };
export type TodoId = number & { readonly __tag: unique symbol };
export type UserId = number & { readonly __tag: unique symbol };

export interface Post {
    userId: UserId;
    id: PostId;
    title: string;
    body: string;
}
export interface Comment {
    postId: PostId;
    id: CommentId;
    name: string;
    email: string;
    body: string;
}
export interface Album {
    userId: UserId;
    id: AlbumId;
    title: string;
}
export interface Photo {
    albumnId: AlbumId;
    id: PhotoId;
    title: string;
    url: string;
    thumbnailUrl: string;
}
export interface Todo {
    userId: UserId;
    id: TodoId;
    title: string;
    completed: boolean;
}
export interface User {
    id: UserId;
    name: string;
    username: string;
    email: string;
    address: Address;
    phone: string;
    company: Company;
}

export interface Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: Geolocation;
}
export interface Geolocation {
    lat: string;
    lng: string;
}
export interface Company {
    name: string;
    catchPhrase: string;
    bs: string;
}