import { Injectable } from '@angular/core';
import { ApiClientService } from './api-client.service';
import { HttpParams } from '@angular/common/http';
import { User, UserId } from './interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersClientService {

  constructor(private apiClient: ApiClientService) { }

  getAll(): Observable<User[]> {
    return this.apiClient.get("/users");
  }

  getByIds(ids: UserId[]): Observable<User[]> {
    const params = new HttpParams();
    for (const id of ids) {
      params.append("userId", id);
    }
    return this.apiClient.get("/users", params);
  }

}
