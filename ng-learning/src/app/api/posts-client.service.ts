import { Injectable } from '@angular/core';
import { Post } from './interfaces';
import { Observable } from 'rxjs';
import { ApiClientService } from './api-client.service';

@Injectable({
  providedIn: 'root',
})
export class PostsClientService {

  constructor(private apiClient: ApiClientService) { }

  getAll(): Observable<Post[]> {
    return this.apiClient.get("/posts");
  }

}