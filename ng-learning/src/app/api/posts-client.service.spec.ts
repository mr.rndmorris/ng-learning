import { TestBed } from '@angular/core/testing';

import { PostsClientService } from './posts-client.service';

describe('PostsClientService', () => {
  let service: PostsClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostsClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
